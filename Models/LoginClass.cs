﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;

namespace Quizz_KamiliaSbahi.Models
{
    public class LoginClass
    {
        [Key]

        public int IdClient { get; set; }

        [Required]
        [Display(Name ="Nom")]
        public string Nom { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Username")]
        [Required(ErrorMessage = "Entrez le username.")]
        public string Username { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Rentrez un mot de passe.")]
        public string Password { get; set; }
    }

    public class SearchUser
    {
        public string SearchU(LoginClass loginClass)
        {
            DataClassesDataContext DB = new DataClassesDataContext();
            Client C = new Client();
            string passout = "";
            var pass = from x in DB.Clients where x.Username == loginClass.Username select x.Password;

            foreach (string query in pass)
            {
                passout = query;
            }

            return passout;
        }
    }
}