﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Quizz_KamiliaSbahi.Models;

namespace Quizz_KamiliaSbahi.Controllers
{
    public class ClientController : Controller
    {
        DataClassesDataContext DB = new DataClassesDataContext();
        // GET: Client
        public ActionResult Index()
        {
            return View();
        }

        // GET: Client/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Client/Create
        public ActionResult PrendreRDV()
        {
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        public ActionResult PrendreRDV(RDV collection)
        {
            try
            {
                // TODO: Add insert logic here
                DB.RDVs.InsertOnSubmit(collection);
                DB.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Delete/5
        public ActionResult Delete(int id)
        {
            var databyid = DB.RDVs.Single(x => x.IdRDV == id);
            return View(databyid);
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, RDV collection)
        {
            try
            {
                // TODO: Add delete logic here
                var data = DB.RDVs.Single(x => x.IdRDV == id);
                DB.RDVs.DeleteOnSubmit(data);
                DB.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
