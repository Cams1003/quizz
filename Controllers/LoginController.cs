﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Quizz_KamiliaSbahi.Models;

namespace Quizz_KamiliaSbahi.Controllers
{
    public class LoginController : Controller
    {
        DataClassesDataContext DB = new DataClassesDataContext();
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginClass login)
        {
            SearchUser SS = new SearchUser();
            string pass = SS.SearchU(login);

            if (pass == login.Password)
            {
                var usrE = DB.Clients.Where(x => x.Username == login.Username && x.Password == login.Password).FirstOrDefault();
                if (usrE != null)
                {
                    FormsAuthentication.SetAuthCookie(usrE.Username, false);
                    Session["UserID"] = login.IdClient.ToString();
                    Session["UserPrenom"] = login.Nom.ToString();
                    Session["Password"] = login.Password.ToString();
                }
                return View("Index", "Enseignant");
                //return View("~/Views/LoginUser/Login.cshtml", Item);

            }
            else
            {
                ViewBag.data = "invalid user";
                return View("Login", "Login");
            }
        }

        // GET: Login/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Login/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Login/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Login/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Login/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Login/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Login/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
