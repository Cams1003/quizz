﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Quizz_KamiliaSbahi.Controllers
{
    public class GaragisteController : Controller
    {
        // GET: Garagiste
        public ActionResult ListeRDV()
        {
            return View();
        }

        // GET: Garagiste/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Garagiste/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Garagiste/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Garagiste/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Garagiste/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Garagiste/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Garagiste/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
