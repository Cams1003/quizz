﻿using System.Web;
using System.Web.Mvc;

namespace Quizz_KamiliaSbahi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
